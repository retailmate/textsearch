package com.cognizant.retailmate.search;

import android.content.Intent;
import android.graphics.Typeface;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    Animation slide, slide_close;
    View search_action;
    AutoCompleteTextView query;
    RequestQueue queue;
    private ArrayAdapter<String> adaptersearch;
    private ArrayList<String> suggestions;
    List<SuggestGetSet_findprod> ListData = new ArrayList<>();
    ImageView search_query, search, ar, close, mic;
    private String searchtext, searchId;
    private String token_auth="eyJhbGciOiJSUzI1NiIsImtpZCI6IjU2MjI2YWVlZTNkMWZiZGFhOTQxNGVlMGFkZjdhZDQ4MTY1MDJiYjEifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJpYXQiOjE0ODg1MTEzMDcsImV4cCI6MTQ4ODUxNDkwNywiYXVkIjoiNjU2NTUxODMwMDYwLXVpaWptb3A3YWxvbGR1NjdlbGlpOXZoaGM4cWNuazR0LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTE3NTYzNjc1MDA4NDE4MDA2NTA3IiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImF6cCI6IjY1NjU1MTgzMDA2MC1odjM2ZWthdXFrNzFkaW40bjVqNTcxcGRnMzFodHU5OS5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsImVtYWlsIjoicmV0YWlsbWF0ZWF4QGdtYWlsLmNvbSIsIm5hbWUiOiJBbWVyIEhhc2FuIiwicGljdHVyZSI6Imh0dHBzOi8vbGgzLmdvb2dsZXVzZXJjb250ZW50LmNvbS8tMjkwbUZGX1lIaUkvQUFBQUFBQUFBQUkvQUFBQUFBQUFBQUEvQUFvbXZWM19hOVhUVXhjYlVDbVY3MTY5eHZXLWE4T2lPQS9zOTYtYy9waG90by5qcGciLCJnaXZlbl9uYW1lIjoiQW1lciIsImZhbWlseV9uYW1lIjoiSGFzYW4iLCJsb2NhbGUiOiJlbiJ9.ub0VBiQt6beo1r30kZnwzzsaOIOIvcPYOyVw6rReowMUim14dfmP1UDMn5rhV66cSqqr_BtHBeTJyBINRAse2YaYVb4vMBZAlzyIkQwBHN0Xsf8aQRPCBq8FY6VakK4nYdg1MN7mTFm5Bv_NoQHwO_r98yfhCye3zyjG0byo28V6Gpon-4SmCxenkUz5rIvTmCNnp_zMveJ2V6Uw0rAd7AnjQ3krIos5XssNoq3aorLl8BDLh_MKGkQdpt8aX8tNH4TPR7xi3RwyRg-xWjZMn9hSnP9rULiTEv3Dj5GYEgy5wVJ9bn75ptIyzczmpoiUzl3fm7OUm9yh1Ar8ETXNMQ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        queue = Volley.newRequestQueue(getApplicationContext());

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        search = (ImageView) findViewById(R.id.search);
        ar = (ImageView) findViewById(R.id.ar);

        search_action = findViewById(R.id.search_action);


        slide = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_left);

        slide_close = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_right);

        ar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Augmented Reality", Toast.LENGTH_SHORT).show();
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search.setEnabled(false);
                searchId = "";
                searchtext = "";

                adaptersearch = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line) {
                };


                startsearch();
            }
        });
    }

    private void startsearch() {
        search_action.setVisibility(View.VISIBLE);
        search_action.startAnimation(slide);
//        Toast.makeText(MainActivity.this, "Search", Toast.LENGTH_SHORT).show();

        close = (ImageView) findViewById(R.id.close);
        mic = (ImageView) findViewById(R.id.mic);
        search_query = (ImageView) findViewById(R.id.searchquery);
        query = (AutoCompleteTextView) findViewById(R.id.query);

        query.setText("");
        doInBackground("");

        query.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                query.setTypeface(Typeface.DEFAULT);
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });

        query.setFocusable(false);


        query.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (query.length() == 2) {
                    doInBackground(s.toString());
                } else {
                    adaptersearch.getFilter().filter(s.toString());
                    adaptersearch.setNotifyOnChange(true);
                    query.setAdapter(adaptersearch);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (query.length() > 2) {
                    adaptersearch.getFilter().filter(query.getText().toString());
                    adaptersearch.setNotifyOnChange(true);
                    query.showDropDown();
                }

            }
        });

        query.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (query.length() > 2) {
                    adaptersearch.getFilter().filter(query.getText().toString());
                    adaptersearch.setNotifyOnChange(true);
                    adaptersearch.notifyDataSetChanged();
                    query.showDropDown();
                }
            }
        });

        mic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displaySpeechRecognizer();
//                Toast.makeText(MainActivity.this, "Mic", Toast.LENGTH_SHORT).show();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (query.getText().toString().equals("")) {
                    search_action.startAnimation(slide_close);
                    search_action.setVisibility(View.INVISIBLE);
                    search.setEnabled(true);
                } else
                    query.setText("");
//                Toast.makeText(MainActivity.this, "Close", Toast.LENGTH_SHORT).show();
            }
        });

        query.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                searchtext = query.getText().toString();
                searchId = "";
                int index = adaptersearch.getPosition(searchtext);
                try {
                    searchId = suggestions.get(index);
                } catch (NullPointerException | ArrayIndexOutOfBoundsException e) {
                    Log.e("Error", e.getMessage());
                }


                Toast.makeText(MainActivity.this, "Name:" + searchtext + "\nId:" + searchId, Toast.LENGTH_SHORT).show();
            }
        });

        query.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Toast.makeText(MainActivity.this, "Name:" + searchtext + "\nId:" + searchId, Toast.LENGTH_SHORT).show();
                    return true;
                }
                return false;
            }
        });

        search_query.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("@@## On Tap", String.valueOf(adaptersearch.getCount()));
                if (!searchId.equals(""))
                    Toast.makeText(MainActivity.this, "Name:" + searchtext + "\nId:" + searchId, Toast.LENGTH_SHORT).show();


            }
        });
    }

    private static final int SPEECH_REQUEST_CODE = 0;

    private void displaySpeechRecognizer() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        startActivityForResult(intent, SPEECH_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SPEECH_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> results = data
                    .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            final String spokenText = results.get(0);
            Log.e("@@## spokenText ", spokenText);

            query.postDelayed(new Runnable() {
                @Override
                public void run() {

                    query.setText(spokenText);
                    query.setFocusable(true);
                    query.setSelection(query.getText().length());
                    doInBackground(query.getText().toString().trim());
                    Log.e("@@## After Speech", String.valueOf(adaptersearch.getCount()) + " " + adaptersearch.toString());
                    adaptersearch.getFilter().filter(query.getText().toString());
                    adaptersearch.setNotifyOnChange(true);
                    adaptersearch.notifyDataSetChanged();
                    query.showDropDown();
                }
            }, 500);
            query.setFocusable(true);
            query.setFocusableInTouchMode(true);
            query.requestFocus();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void doInBackground(String query_text) {
        ListData = new ArrayList<>();

        Log.e("@@## before CALL", query_text);
        String url = "https://JDARetailMatedevret.cloudax.dynamics.com/Commerce/Products/SearchByText(channelId=68719476778,catalogId=0,searchText="
                + "\'" + query_text + "\'" + ")?$top=20&api-version=7.1";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("@@##", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("value");
                            Log.e("@@## Json Array", String.valueOf(jsonArray.length()));

                            for (int i = 0; i < jsonArray.length(); i++) {
                                jsonObject = jsonArray.getJSONObject(i);
                                ListData.add(new SuggestGetSet_findprod(jsonObject
                                        .getString("RecordId"), jsonObject
                                        .getString("Name"), jsonObject
                                        .getString("ItemId")));
                            }

                            Log.e("@@## ListData", String.valueOf(ListData.size()));

                            if (ListData.size() > 0) {
                                adaptersearch.clear();

                                suggestions = new ArrayList<>();
                                suggestions.clear();

                                for (int i = 0; i < ListData.size(); i++) {
                                    suggestions.add(ListData.get(i).getId());
                                }

                                Log.e("@@## Suggestions", String.valueOf(suggestions.size()));

                                for (int i = 0; i < ListData.size(); i++) {
                                    adaptersearch.add(ListData.get(i).getName());
                                }
                                adaptersearch.setNotifyOnChange(true);
                                query.setAdapter(adaptersearch);
                                query.showDropDown();
                                adaptersearch.notifyDataSetChanged();

                                if (query.length() > 2) {
                                    adaptersearch.getFilter().filter(query.getText().toString());
                                    adaptersearch.setNotifyOnChange(true);
                                    adaptersearch.notifyDataSetChanged();
                                    query.showDropDown();
                                }


                            }

                        } catch (NullPointerException | ArrayIndexOutOfBoundsException | JSONException e) {
                            Log.e("Error", e.getMessage());
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                adaptersearch.clear();
                adaptersearch.add("Connection Error");
                query.setAdapter(adaptersearch);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "id_token "+token_auth);
                params.put("OUN", "094");
                return params;
            }
        };
        queue.add(stringRequest);

    }

}
