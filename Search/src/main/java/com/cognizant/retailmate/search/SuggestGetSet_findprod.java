package com.cognizant.retailmate.search;

/**
 * Created by 599654 on 2/28/2017.
 */
public class SuggestGetSet_findprod {

    String RecordId,Name,ItemId;

    public SuggestGetSet_findprod(String RecordId,String Name,String ItemId){
        this.RecordId=RecordId;
        this.Name=Name;
        this.ItemId=ItemId;
    }

    public String getRecordId() {
        return RecordId;
    }

    public void setRecordId(String recordId) {
        RecordId = recordId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getId() {
        return ItemId;
    }

    public void setItemId(String itemId) {
        ItemId = itemId;
    }
}
